
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
    
    
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('linkserver.db', (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to the in-memory SQlite database.');
});

db.serialize(function() {
    db.run("CREATE TABLE IF NOT EXISTS registrations (key TEXT, port INTEGER)");
    db.run("INSERT INTO registrations (key, port) VALUES (?, ?)", "dummy", 3000);
});


var request = require('request');
var express = require('express');
var morgan = require('morgan')
var restapi = express();

restapi.get('/registration/:key', function(req, res){
    if (req.params.key) {
        db.get("SELECT port FROM registrations WHERE key = ?", req.params.key, function(err, row){
            if (!row) {
                res.status(404);
            }
            else if (err){
                console.log(err);
                res.status(500);
            }
            else {
                res.json({ "port" : row.port });
                res.status(200);
            }
            res.end();
        });
    }
    else {
        res.json({"error": "you need to provide a registration key"});
        res.status(400);
    }
});

restapi.post('/registration', function(req, res){
    
    db.get("SELECT port FROM registrations ORDER BY port DESC", function(err, row){
        if (row) {
            let newport = row.port + 1;
            let newkey = uuidv4();
            console.log("new key is: " + newkey + " and new port is " + newport);
    
            request.post('http://proxy:8001/api/routes/'+newkey+'.localhost', {'json': true, 'body': {'target': 'http://ssh:'+newport}}, function (error, response, body) {
                if(response && response.statusCode == 201){
                    db.run("INSERT INTO registrations (port, key) VALUES (?, ?)", newport, newkey, function(err, row){
                        if (err){
                            console.log(err);
                            res.status(500);
                        }
                        else {
                            res.json({ "key": newkey, "port" : newport });
                            res.status(202);
                        }
                        res.end();
                    });
                } else {
                    console.log('error while setting route: '+ response); //.statusCode
                    console.log(body);
                    res.status(500);
                }               
            });
            
        }
    });
});

restapi.use(morgan('combined'));
restapi.use(express.static('static'));
restapi.listen(3000);

console.log("Submit GET or POST to http://localhost:3000/registration");
