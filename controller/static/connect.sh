#!/bin/bash

die () {
	echo "Usage: $0 <port>"
	echo >&2 "$@"
	exit 1
}

# parameter validation
[ "$#" -eq 1 ] || die "required argument <port> is missing"
echo $1 | grep -E -q '^[0-9]+$' || die "<port> must be a numeric value, $1 provided"

LOCAL_PORT=$1
#PUBLIC_DOMAIN="linkserver.superkartoffel.de"
PUBLIC_DOMAIN="localhost"

# check if registration file already exists
if [ ! -f registration.json ]; then
	# register new device
	curl -X POST http://$PUBLIC_DOMAIN:3000/registration -o registration.json
fi

# parse the registration
REMOTE_PORT=$(grep -oP '(?<="port":)[0-9]+' registration.json)
REMOTE_USER=tunnel
#REMOTE_USER=$(grep -oP '(?<="key":").*?[^\\](?=",)' registration.json)

# connect via ssh
ssh $REMOTE_USER@$PUBLIC_DOMAIN -p 2200 -R $REMOTE_PORT:localhost:$LOCAL_PORT -N
